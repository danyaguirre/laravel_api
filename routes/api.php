<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post("register", "Api\AuthController@register");
Route::post("test", "Api\AuthController@test");

Route::group(["middleware" => "auth:api"], function(){
    Route::post("testOauth", "Api\AuthController@testOauth");
    Route::get("getUsers", "Api\UserdataController@getUsers");
    Route::get("getUsers/{idd}", "Api\UserdataController@getUserDetail");
    Route::post("user/register", "Api\UserdataController@addUsers");
    Route::put("user/modificar", "Api\UserdataController@updateUsers");
    Route::delete("user/delete", "Api\UserdataController@deleteUsers");

    
});
Route::get("actividad", "Api\ActividadController@getActividades");
Route::get("actividad/{idd}", "Api\ActividadController@getActividadDetail");
Route::post("actividad/register", "Api\ActividadController@addActividad");
Route::put("actividad/modificar", "Api\ActividadController@updateActividad");
Route::put("actividad/active", "Api\ActividadController@deleteActividad");

Route::get("confirmacion", "Api\ConfirmacionController@geConfirmacion");
Route::get("confirmacion/{idd}", "Api\ConfirmacionController@getConfirmacionDetail");
Route::get("confirmacion/user/{idd}", "Api\ConfirmacionController@getConfirmacionUser");
Route::post("confirmacion/register", "Api\ConfirmacionController@addConfirmacion");
Route::put("confirmacion/modificar", "Api\ConfirmacionController@updateConfirmacion");
Route::put("confirmacion/active", "Api\ConfirmacionController@deleteConfirmacion");

Route::put("getUsers/addOneSignal/{idd}", "Api\UserdataController@addOneSignal");

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
