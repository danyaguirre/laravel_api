<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateConfirmacion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Confirmacion', function (Blueprint $table) {
            $table->increments("id");
            $table->integer("iduser");
            $table->integer("idactividad");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Schema::table('Confirmacion', function (Blueprint $table) {
        //     //
        // });
        Schema::dropIfExists('Confirmacion');
    }
}
