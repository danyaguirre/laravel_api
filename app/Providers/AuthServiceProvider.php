<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;
use Laravel\Passport\Passport;
use Carbon\Carbon;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    //inicializa el sistema de authenticacion
    public function boot()
    {
        $this->registerPolicies();
        //le explicamos como tiene que trabajar. Para que nos devulva las rutas relacionadas con este passport.
        // Continuamos en config/auth.php
        Passport::routes(function($router){
            $router->forAccessTokens();
            $router->forPersonalAccessTokens();
            $router->forTransientTokens();
        });

        Passport::tokensExpireIn(Carbon::now()->addMinutes(100));
        Passport::refreshTokensExpireIn(Carbon::now()->addDays(100));
        //
    }
}
