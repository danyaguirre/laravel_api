<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Userdata extends Model
{
    protected $table = "userdata";
    public $timestamps = false;

    public function userAccess(){
        return $this->hasOne("App\User", "id", "iduser")->select(["id", "email"]);
    }
}
