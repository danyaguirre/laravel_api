<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Api\ApiController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use Validator;

class AuthController extends ApiController
{
    public function testOauth(Request $request){
        return $this->sendResponse(Auth::user());
    }

    public function test(Request $request){
        return $this->sendResponse(["status" => "OKMAKEY"], "Test funciona con exito");
    }

    public function register(Request $request){
        $validator = Validator::make(
            $request->all(), 
            [
                "name" => "required",
                "email" => "required|email",
                "password" => "required",
                "confirm_password" => "required|same:password"
            ]
        );

        if($validator->fails()){
            return $this->sendError("Error de Validacion", $validator->errors(), 422);
        }

        $input = $request->all();
        $input["password"] = bcrypt($request->get("password"));

        $user = User::create($input);
        $token = $user->createToken("MyApp")->accessToken;

        return $this->sendResponse(
            [
                "token" => $token,
                "user" => $user
            ],
            "Usuario creado con exito!!"
        );
    }
}
