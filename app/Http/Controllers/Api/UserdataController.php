<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Api\ApiController;
use Illuminate\Http\Request;
Use App\Userdata;
use App\User;
use Validator;
Use Illuminate\Support\Facades\DB;

class UserdataController extends ApiController
{
    public function getUsers(){
        return $this->sendResponse(
            Userdata::get(),
            "Usuarios recuperados correctamente"
        );
    }

    public function getUserDetail($idd, Request $request){
        // $data = Userdata::where("iduser", "=", "1")->with("userAccess")->select("nombre", "foto", "edad", "genero", "iduser")->get();
        $data["user"] = DB::table("users")
                ->join("userdata", "users.id", "=", "userdata.iduser")
                ->select("users.id", "userdata.nombre", "userdata.foto", "userdata.edad", "userdata.genero")
                ->get();
        return $this->sendResponse($data);
    }

    public function addUsers(Request $request){
        $validator = Validator::make(
            $request->all(), 
            [
                "name" => "required",
                "email" => "required|email|unique:users",
                "password" => "required",
                "confirm_password" => "required|same:password",
                "edad" => "required",
                "genero" => "required",
                "acercade" => "required"
            ]
        );

        if($validator->fails()){
            return $this->sendError("Error de Validacion", $validator->errors(), 422);
        }

        $input = $request->all();
        $input["password"] = bcrypt($request->get("password"));

        $user = User::create($input);
        $token = $user->createToken("MyApp")->accessToken;

        $userData = new Userdata();
        $userData->nombre = $request->get("name");
        $userData->foto = $request->get("foto");
        $userData->edad = $request->get("edad");
        $userData->genero = $request->get("genero");
        $userData->acercade = $request->get("acercade");
        $userData->iduser = $user->id;
        $userData->save();

        return $this->sendResponse(
            [
                "token" => $token,
                "user" => $user,
                "userData" => $userData
            ],
            "Usuario creado con exito!!"
        );
    }

    public function updateUsers(Request $request){
        $user = User::find($request->get("id"));

        if($user == null){
            return $this->sendError("Error en los datos", ["El usuario no existe"], 422);
        }

        $validator = Validator::make(
            $request->all(), 
            [
                "id" => "required",
                "name" => "required",
                "edad" => "required",
                "genero" => "required",
                "acercade" => "required"
            ]
        );

        if($validator->fails()){
            return $this->sendError("Error de Validacion", $validator->errors(), 422);
        }

        $user->name = $request->get("name");
        $user->save();

        $userData = Userdata::where("iduser", "=", $user->id)->first();
        $userData->nombre = $request->get("name");
        $userData->edad = $request->get("edad");
        $userData->genero = $request->get("genero");
        $userData->acercade = $request->get("acercade");
        $userData->save();

        return $this->sendResponse(
            [
                "user" => $user,
                "userData" => $userData
            ],
            "Usuario modificado con exito!!"
        );        
    }

    public function deleteUsers(Request $request){
        $user = User::find($request->get("id"));
        if($user == null){
            return $this->sendError("Error en los datos", ["El usuario no existe"], 422);           
        }

        $user->delete();

        $userData = Userdata::where("iduser", "=", $request->get("id"))->first();
        $userData->delete();

        return $this->sendResponse(
            ["status" => "ok"],
            "Usuario borrado correctamente"
        );
    }

    public function addOneSignal($id, Request $request){
        $userdata = Userdata::find($id);

        if($userdata == null){
            return $this->sendError("Error en los datos", ["El usuario no existe"], 422);
        }

        $validator = Validator::make(
            $request->all(), 
            [ "onSignalId" => "required" ]
        );

        if($validator->fails()){
            return $this->sendError("Error de Validacion", $validator->errors(), 422);
        }

        $userdata->idonesignal = $request->get("onSignalId");
        $userdata->save();        

        return $this->sendResponse(
            [ "userData" => $userdata ],
            "Usuario modificado con exito!!"
        );
    }
}