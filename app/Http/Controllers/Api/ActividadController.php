<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Api\ApiController;
use Illuminate\Http\Request;
Use Illuminate\Support\Facades\DB;
use App\Actividad;
use Validator;

class ActividadController extends ApiController
{
    public function getActividades(Request $request){
        $acti["actividades"] = Actividad::select("id", "nombre", "foto", "fecha")->get();
        return $this->sendResponse($acti, "Actividades recuperadas correctamente");
    }

    public function addActividad(Request $request){
        $validator = Validator::make(
            $request->all(), 
            [
                "nombre" => "required",
                "foto" => "required",
                "descripcion" => "required",
                "fecha" => "required"
            ]
        );

        if($validator->fails()){
            return $this->sendError("Error de Validacion", $validator->errors(), 422);
        }

        $actividad = new Actividad();
        $actividad->nombre = $request->get("nombre");
        $actividad->foto = $request->get("foto");
        $actividad->descripcion = $request->get("descripcion");
        $actividad->fecha = $request->get("fecha");
        $actividad->save();

        return $this->sendResponse(
            [ "actividad" => $actividad ],
            "Actividad creado con exito!!"
        );
    }

    public function deleteActividad(Request $request){
        $acti = Actividad::find($request->get("id"));
        if($acti == null){
            return $this->sendError("Error en los datos", ["La actividad no existe"], 422);
        }

        $acti->active = $request->get("active");
        $acti->save();

        return $this->sendResponse(
            ["status" => "ok"],
            "Actividad modificada correctamente"
        );
    }

    public function getActividadDetail($id, Request $request){
        $data["actividad"] = Actividad::find($id);
        if($data["actividad"] == null){
            return $this->sendError("Error de los datos", "La actividad no existe", 422);
        }

        $data["users"] = DB::table("confirmacion")
                ->where("confirmacion.idactividad", "=", $id)
                ->join("userdata", "confirmacion.iduser", "userdata.iduser")
                ->select("userdata.iduser", "userdata.nombre" , 
                        "userdata.foto", "userdata.edad", "userdata.genero")
                ->get();

        return $this->sendResponse($data);
    }

    public function updateActividad(Request $request){
        $actividad = Actividad::find($request->get("id"));
        if($actividad == null){
            return $this->sendError("Error en los datos", ["La actividad no existe"], 422);
        }
        
        $validator = Validator::make(
            $request->all(), 
            [
                "id" => "required",
                "nombre" => "required",
                "foto" => "required",
                "descripcion" => "required",
                "fecha" => "required"
            ]
        );

        if($validator->fails()){
            return $this->sendError("Error de Validacion", $validator->errors(), 422);
        }

        $actividad->nombre = $request->get("nombre");
        $actividad->foto = $request->get("foto");
        $actividad->descripcion = $request->get("descripcion");
        $actividad->fecha = $request->get("fecha");
        $actividad->save();

        return $this->sendResponse(
            [ "actividad" => $actividad ],
            "Actividad modificado con exito!!"
        );
    }
}
