<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Api\ApiController;
use Illuminate\Http\Request;
Use Illuminate\Support\Facades\DB;
use App\Actividad;
use App\Confirmacion;
use Validator;
use OneSignal;

class ConfirmacionController extends ApiController
{
    public function geConfirmacion(Request $request){
        $confi["confirmacion"] = Confirmacion::get();
        return $this->sendResponse($confi, "Confirmaciones recuperadas correctamente");
    }

    public function addConfirmacion(Request $request){
        $validator = Validator::make(
            $request->all(), 
            [
                "iduser" => "required",
                "idactividad" => "required"
            ]
        );

        if($validator->fails()){
            return $this->sendError("Error de Validacion", $validator->errors(), 422);
        }
        $confirmacion = Confirmacion::where("iduser", "=", $request->get("iduser"))
                        ->where("idactividad", "=", $request->get("idactividad"))
                        ->first();

        if($confirmacion != null){
            return $this->sendError("Datos invalidos", ["El usuario ya confirmo previamente"], 422);
        }
        // $confirmacion = Confirmacion::create($request->all());
        $confirmacion = new Confirmacion();
        $confirmacion->iduser = $request->get("iduser");
        $confirmacion->idactividad = $request->get("idactividad");
        $confirmacion->save();

        $users = DB::table("confirmacion")
                ->where("confirmacion.idactividad", "=", $confirmacion->idactividad)
                ->join("userdata", "confirmacion.iduser", "userdata.iduser")
                ->select("userdata.idOneSignal", "userdata.iduser", "userdata.nombre", "userdata.foto", "userdata.edad", "userdata.genero")
                ->get();

        foreach($users as $user){
            $idOneSignal = $user->idOneSignal;
            if($idOneSignal != 1){
                OneSignal::sendNotificationToUser(
                    "Se ha aputando otro usuario a la actividad",
                    $idOneSignal,
                    $url = null,
                    $data = null,
                    $buttons = null,
                    $schedule = null
                );
            }
        }

        return $this->sendResponse(
            [ "confirmacion" => $confirmacion, "OneSignal" => $idOneSignal ],
            "Confirmacion creada con exito!!"
        );
    }

    public function deleteConfirmacion(Request $request){
        $confirmacion = Confirmacion::find($request->get("id"));
        if($confirmacion == null){
            return $this->sendError("Error en los datos", ["La confirmacion no existe"], 422);
        }
        $confirmacion->delete();

        return $this->sendResponse(
            ["status" => "ok"],
            "Confirmacion borrada correctamente"
        );
    }

    public function getConfirmacionDetail($id, Request $request){
        $confirmacion = Confirmacion::find($id);
        if($confirmacion == null){
            return $this->sendError("Error en los datos", ["La confirmacion no existe"], 422);
        }

        $actividad = Actividad::find($confirmacion->idactividad);
        if($actividad == null){
            return $this->sendError("Error en los datos", ["La actividad no existe"], 422);
        }

        $users = DB::table("confirmacion")
                ->where("confirmacion.idactividad", "=", $confirmacion->idactividad)
                ->join("userdata", "confirmacion.iduser", "userdata.iduser")
                ->select("userdata.iduser", "userdata.nombre", "userdata.foto", "userdata.edad", "userdata.genero")
                ->get();


        return $this->sendResponse(
            [ "actividad" => $actividad, "users" => $users ],
            "Confirmacion recuperada con exito!!"
        );
    }

    public function getConfirmacionUser($id, Request $request){
        $confirmaciones = DB::table("confirmacion")
                ->where("confirmacion.iduser", "=", $id)
                ->join("actividad", "confirmacion.idactividad", "actividad.id")
                ->select("confirmacion.id", "confirmacion.idactividad" , 
                        "actividad.nombre", "actividad.active", "actividad.fecha",
                        "actividad.descripcion", "actividad.foto")
                ->get();

        return $this->sendResponse(
            [ "confirmaciones" => $confirmaciones ],
            "Confirmaciones recuperadas con exito!!"
        );
    }

    public function updateActividad(Request $request){
        $actividad = Actividad::find($request->get("id"));
        if($actividad == null){
            return $this->sendError("Error en los datos", ["La actividad no existe"], 422);
        }
        
        $validator = Validator::make(
            $request->all(), 
            [
                "id" => "required",
                "nombre" => "required",
                "foto" => "required",
                "descripcion" => "required",
                "fecha" => "required"
            ]
        );

        if($validator->fails()){
            return $this->sendError("Error de Validacion", $validator->errors(), 422);
        }

        $actividad->nombre = $request->get("nombre");
        $actividad->foto = $request->get("foto");
        $actividad->descripcion = $request->get("descripcion");
        $actividad->fecha = $request->get("fecha");
        $actividad->save();

        return $this->sendResponse(
            [ "actividad" => $actividad ],
            "Actividad modificado con exito!!"
        );
    }
}
